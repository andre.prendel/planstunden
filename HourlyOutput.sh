#!/bin/bash

getFile()
{
    awk '!/^#/' "$1"
}

for f in $(ls -1); do
    echo "$f"

    i=0
    store=0
    for row in $(getFile $f); do
        #echo "$i: $row"
        if [ $i -eq 0 ]; then
            store=$row
        else
            if [ ! "$row" = "-" ]; then
                json="{ \"HourlyOutput\": $row }"
                curl -k -v -X PUT -H "Content-Type: application/json" -d "$json" https://localhost:5001/StatisticsPlan/store/$store/year/2020/month/$i
            fi
        fi
        i=$(($i+1))
    done
done

exit 0
