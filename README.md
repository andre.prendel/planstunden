# Hourly output (Stundenleistung)

This project contains text files for almost any store, which in turn contains the planned hourly output for this store. The numbers comes from the excel sheets you can find [here](https://pthgroup.atlassian.net/wiki/spaces/SYSTEME/pages/613875721/Planstunden+2020).

Further in this repository there is a [script](https://gitlab.com/andre.prendel/planstunden/-/blob/master/HourlyOutput.sh) which parses the files and generates an REST (PUT) calls against the [budget-api](https://gitlab.com/pthgroup/isp/budget-api) to update the planned working hours (Planstunden) based on the hourly output (Stundenleistung) found in the file.

The format of the files is pretty simple. In the first line there is a comment (starts with '#'), which just states the Hiltes store id and the name of the store found in the excel sheet. On the second line is the store id from ISP. The third line just contains a delimiter ('#') to separate the store meta data from the hourly output numbers.

Here is an example of `Tom Tailor Dresden AMG` with Hiltes store id 1032 and ISP store id 135 (this relation has to be define manually in the files).

```
# 1302 - Dresden AMG    # This is the Hiltes store id and the name from the excel sheet (it's actually not used be the script).
135                     # This is the ISP store id, it's used by the script.
#                       # This is just a delimiter to make the file more readable. It's thrown away by the script.
83                      # Now follows the hourly output for all the months of the year.
80
85
120
115
122
98
107
104
152
162
154
```

To update some of the hourly output numbers, you have to run the script in the according brand folder and of course you need a running [budget-api](https://gitlab.com/pthgroup/isp/budget-api).

```
# cd budget-api
# dotnet run --ConnectionString:Server=W2K3SQL --ConnectionString:Database=PTH --ConnectionString:Username=USERNAME --ConnectionString:Password=PASSWORD
# cd ..
#
# cd CK
# ../HourlyOutput.sh                                                            
...
# > PUT /StatisticsPlan/store/393/year/2020/month/1 HTTP/2                                         
# > Host: localhost:5001                                      
# > User-Agent: curl/7.58.0                                                                        
# > Accept: */*                                                                   
# > Content-Type: application/json                            
# > Content-Length: 22                                                            
# >                                                                                                
# * We are completely uploaded and fine                    
# * Connection state changed (MAX_CONCURRENT_STREAMS updated)!                                     
# < HTTP/2 204                                                                                     
# < date: Mon, 17 Aug 2020 09:18:41 GMT                       
# < server: Kestrel
...
```
